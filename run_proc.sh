#!/usr/bin/env bash

set -e

DEVICE=$1
: ${DEVICE:=/dev/video0}

DIR="$(realpath $( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd ))"

#enable Xserver inside docker
ifaces=$(ifconfig | grep -o "^en[^ ]*:\|^eth[^ ]*" | grep -o "[^:]*" | paste -s -d ' ' -)
ip_addr=$(for if in ${ifaces}; do
    ifconfig ${if} | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | \
    grep -v '127.0.0.1' | head -1
done | head -1)
xhost +${ip_addr}

PIPELINE="v4l2src device=$DEVICE ! decodebin ! duration ! xvimagesink"

echo "using pipeline: $PIPELINE"

docker run -it --rm \
  -e DISPLAY=$DISPLAY \
  -e GST_DEBUG=2 \
  -v /tmp/.X11-unix:/tmp/.X11-unix \
  -v "${DIR}/python:/home/gst/python:ro" \
  --privileged=true \
  --entrypoint bash \
  tabletennis \
  -c "gst-launch-1.0 $PIPELINE; gst-launch-1.0 $PIPELINE"
