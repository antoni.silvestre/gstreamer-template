FROM debian:sid

ENV PYTHON=/usr/bin/python3

RUN apt-get update && apt-get upgrade -y && apt-get install -y --no-install-recommends \
    autoconf `# libnice` \
    automake `# libnice` \
    bison \
    build-essential \
    ca-certificates \
    flex \
    gettext \
    git \
    gnutls-dev `# libnice` \
    gtk-doc-tools `# libnice` \
    libffi-dev \
    libgirepository1.0-dev \
    libglib2.0 \
    libnice-dev \
    libopus-dev \
    libpcre3-dev \
    libjpeg-dev \
    libpng-dev \
    libssl-dev `# needed for DTLS requirement`\
    libtool `# libnice` \
    libvpx-dev \
    libx264-dev \
    libxv-dev \
    libgl1-mesa-dev \
    libglu1-mesa-dev \
    freeglut3-dev \
	libmjpegtools-dev \
    locales \
    libavcodec-dev \
    libavdevice-dev \
    libavfilter-dev \
    libavformat-dev \
    libavresample-dev \
    libavutil-dev \
	python3-pip \
	python3-dev \
	python3-gi \
	python3-setuptools \
	python-gi-dev \
    mount \
    perl \
    curl \
    python \
    wget \
    libpango1.0-dev \
    zlib1g && apt-get clean

ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US.UTF-8

RUN echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen && locale-gen en_US.utf8 && /usr/sbin/update-locale LANG=en_US.UTF-8

ARG GST_VER=1.15.2
ARG CORES=8
RUN mkdir -p /src
RUN mkdir -p /build

ARG PREF=/opt/gstreamer

ENV PATH=${PREF}/bin:$PATH
ENV PKG_CONFIG_PATH=${PREF}/lib/pkgconfig/:$PKG_CONFIG_PATH
ENV LD_LIBRARY_PATH=${PREF}/lib:$LD_LIBRARY_PATH
ENV GI_TYPELIB_PATH=${PREF}/lib/girepository-1.0:$GI_TYPELIB_PATH
ENV PYTHONPATH=${PREF}/lib/python3.7/:$PYTHONPATH

RUN printf "${PREF}/lib/python3.7/site-packages\n${PREF}/lib/python3.7/dist-packages" \
  > /usr/lib/python3/dist-packages/gstreamer.pth

#install ninja-build
WORKDIR /src
RUN curl -L https://github.com/ninja-build/ninja/archive/v1.8.2.tar.gz | tar xzvf -

WORKDIR /src/ninja-1.8.2
RUN python3 configure.py --bootstrap
RUN mkdir -p ${PREF}/bin
RUN install -vm755 ninja ${PREF}/bin/ 

#install meson
RUN pip3 install meson

ARG GST_OPTS="-Ddisable_introspection=false -Dgtk_doc=disabled -Ddisable_gtkdoc=true -Ddisable_examples=true"
ARG MSN_OPTS="--prefix=${PREF} --libdir=${PREF}/lib --buildtype=release"

# liborc
WORKDIR /src
RUN curl https://gstreamer.freedesktop.org/src/orc/orc-0.4.28.tar.xz | tar xvfJ -

WORKDIR /src/orc-0.4.28
RUN mkdir -p builddir
RUN cd builddir; meson ${MSN_OPTS} ..
RUN cd builddir; ninja
RUN cd builddir; ninja install

# http://www.linuxfromscratch.org/blfs/view/svn/multimedia/gstreamer10.html
WORKDIR /src
RUN curl https://gstreamer.freedesktop.org/src/gstreamer/gstreamer-${GST_VER}.tar.xz | tar xvfJ -

WORKDIR /src/gstreamer-${GST_VER}
RUN mkdir -p builddir
RUN cd builddir; meson ${MSN_OPTS} ${GST_OPTS} ..
RUN cd builddir; ninja
RUN cd builddir; ninja install

# gst-plugins-base
WORKDIR /src
RUN curl https://gstreamer.freedesktop.org/src/gst-plugins-base/gst-plugins-base-${GST_VER}.tar.xz | tar xvfJ -

WORKDIR /src/gst-plugins-base-${GST_VER}
RUN mkdir -p builddir
RUN cd builddir; meson ${MSN_OPTS} ${GST_OPTS} ..
RUN cd builddir; ninja
RUN cd builddir; ninja install

# libnice
WORKDIR /src
RUN git clone https://github.com/libnice/libnice.git
WORKDIR /src/libnice
RUN ./autogen.sh --prefix=${PREF}  --libdir=${PREF}/lib --with-gstreamer --enable-static --enable-static-plugins --enable-shared \
	--without-gstreamer-0.10 --disable-gtk-doc
RUN make -j $CORES
RUN make -j $CORES install

# gst-plugins-good
WORKDIR /src
RUN curl https://gstreamer.freedesktop.org/src/gst-plugins-good/gst-plugins-good-${GST_VER}.tar.xz | tar xvfJ -

WORKDIR gst-plugins-good-${GST_VER}
RUN mkdir -p builddir
RUN cd builddir; meson ${MSN_OPTS} ${GST_OPTS} ..
RUN cd builddir; ninja
RUN cd builddir; ninja install

# webrtc-audio-processing
WORKDIR /src
RUN curl -L http://freedesktop.org/software/pulseaudio/webrtc-audio-processing/webrtc-audio-processing-0.3.1.tar.xz | \
	tar xvfJ -

WORKDIR webrtc-audio-processing-0.3.1
RUN ./configure --prefix=$PREF
RUN make -j $CORES
RUN make -j $CORES install

# gst-plugins-bad
WORKDIR /src
RUN curl https://gstreamer.freedesktop.org/src/gst-plugins-bad/gst-plugins-bad-${GST_VER}.tar.xz | tar xvfJ -

WORKDIR gst-plugins-bad-${GST_VER}
RUN mkdir -p builddir
RUN cd builddir; meson ${MSN_OPTS} ${GST_OPTS} ..
RUN cd builddir; ninja
RUN cd builddir; ninja install

# gst-plugins-ugly
WORKDIR /src
RUN curl https://gstreamer.freedesktop.org/src/gst-plugins-ugly/gst-plugins-ugly-${GST_VER}.tar.xz | tar xvfJ -

WORKDIR gst-plugins-ugly-${GST_VER}
RUN mkdir -p builddir
RUN cd builddir; meson ${MSN_OPTS} ${GST_OPTS} ..
RUN cd builddir; ninja
RUN cd builddir; ninja install

# gst-rtsp-server
WORKDIR /src
RUN curl https://gstreamer.freedesktop.org/src/gst-rtsp-server/gst-rtsp-server-${GST_VER}.tar.xz | tar xvfJ -

RUN pip3 install --upgrade meson==0.48.2

WORKDIR gst-rtsp-server-${GST_VER}
RUN mkdir -p builddir
RUN cd builddir; meson ${MSN_OPTS} ${GST_OPTS} ..
RUN cd builddir; ninja
RUN cd builddir; ninja install

# gst-libav
WORKDIR /src
RUN curl https://gstreamer.freedesktop.org/src/gst-libav/gst-libav-${GST_VER}.tar.xz | tar xvfJ -

WORKDIR /src/gst-libav-${GST_VER}
RUN mkdir -p builddir
RUN cd builddir; meson ${MSN_OPTS} ${GST_OPTS} ..
RUN cd builddir; ninja
RUN cd builddir; ninja install

RUN apt-get update && apt-get install -y --no-install-recommends \
	  gir1.2-gstreamer-1.0 \
      cmake \
      libjpeg-dev \
      python3-numpy \
      python3-pillow \
      python3-requests \
      vim \
      libcurl3-dev \
      libleptonica-dev \
      liblog4cplus-dev \
      libopencv-dev \
      libtesseract-dev \
      imagemagick \
      libgoogle-perftools-dev \
      && apt-get clean


WORKDIR /src
RUN curl https://gstreamer.freedesktop.org/src/gst-python/gst-python-${GST_VER}.tar.xz | tar xJf - && \
  mv gst-python*/ gst-python

WORKDIR /src/gst-python

ENV PYTHON=/usr/bin/python3
RUN mkdir -p builddir
RUN cd builddir; meson ${MSN_OPTS} ${GST_OPTS} ..
RUN cd builddir; ninja
RUN cd builddir; ninja install

#Compile the SIMD library

WORKDIR /src/
RUN git clone https://github.com/ermig1979/Simd.git

WORKDIR /src/Simd
RUN cd prj/cmake && cmake . -DCMAKE_INSTALL_PREFIX:PATH=${PREF} -DCMAKE_C_COMPILER="/usr/bin/gcc" \
  -DCMAKE_CXX_COMPILER="/usr/bin/g++" -DTOOLCHAIN="/usr/bin/g++" -DTARGET="x86_64" -DCMAKE_BUILD_TYPE="Release" \
  -DLIBRARY="SHARED" -DCMAKE_CXX_FLAGS="-std=c++11" -DCMAKE_C_FLAGS="-std=c++11" -DSIMD_TEST=OFF
RUN cd prj/cmake && make -j ${CORES}
RUN cp prj/cmake/libSimd.so /usr/lib
RUN mkdir -p /build/usr/lib
RUN cp prj/cmake/libSimd.so /build/usr/lib


RUN apt-get install -y --no-install-recommends \
      python3-pip \
      python3-setuptools \
      && apt-get clean

RUN echo "deb http://deb.debian.org/debian sid main non-free" > /etc/apt/sources.list
RUN apt-get update && apt-cache search linux-headers && echo ""
RUN apt-get update && apt-get install -y linux-headers-$(uname -r) nvidia-driver && apt-get clean

ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US.UTF-8

RUN echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen && locale-gen en_US.utf8 && /usr/sbin/update-locale LANG=en_US.UTF-8
ENV TZ="Europe/Madrid"
RUN bash -c "ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone"

ENV PATH=${PREF}/bin:$PATH
ENV PKG_CONFIG_PATH=${PREF}/lib/pkgconfig/:$PKG_CONFIG_PATH
ENV LD_LIBRARY_PATH=${PREF}/lib:$LD_LIBRARY_PATH
ENV GI_TYPELIB_PATH=${PREF}/lib/girepository-1.0:$GI_TYPELIB_PATH
ENV PYTHONPATH=${PREF}/lib/python3.7/:$PYTHONPATH

RUN printf "${PREF}/lib/python3.7/site-packages\n${PREF}/lib/python3.7/dist-packages" \
  > /usr/lib/python3/dist-packages/gstreamer.pth

# Create a non root user
RUN apt-get install sudo && apt-get clean
RUN export uid=1000 gid=1000 && \
    groupadd -g 1000 gst && \
    useradd -g 1000 -u 1000 -d /home/gst -ms /bin/bash gst && \
    mkdir -p /home/gst && \
    echo "gst ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/gst && \
    chmod 0440 /etc/sudoers.d/gst && \
    chown ${uid}:${gid} -R /home/gst && \
    usermod -a -G video gst

RUN mkdir -p ${PREF}/lib/python3.7/dist-packages/

USER gst
RUN mkdir -p /home/gst/
WORKDIR /home/gst/

ENV GST_PLUGIN_PATH=/home/gst:${PREF}/lib/gstreamer-1.0/:$GST_PLUGIN_PATH

ENV SOURCE ""

COPY python /home/gst/python/
COPY pipeline.py /home/gst

ENTRYPOINT ["python3", "pipeline.py"]
