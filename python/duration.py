from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import gi
gi.require_version('GstBase', '1.0')

from gi.repository import Gst, GObject, GstBase
Gst.init(None)

class Duration(GstBase.BaseTransform):
    __gstmetadata__ = ('Change buffer durationh','Transform', 'Change buffer duration', 'Antoni Silvestre')

    __gsttemplates__ = (Gst.PadTemplate.new("src",
                                           Gst.PadDirection.SRC,
                                           Gst.PadPresence.ALWAYS,
                                           Gst.Caps.new_any()),
                       Gst.PadTemplate.new("sink",
                                           Gst.PadDirection.SINK,
                                           Gst.PadPresence.ALWAYS,
                                           Gst.Caps.new_any()))

    __gproperties__ = {
        'duration-ms' : (
            int,
            'duration in milliseconds',
            '''duration in milliseconds''',
            1,
            10000000,
            1000,
            GObject.PARAM_READWRITE),
    }

    def do_get_property(self, prop):
        return getattr(self, prop.name.replace("-", "_"))

    def do_set_property(self, prop, value):
        attr = prop.name.replace("-", "_")
        getattr(self, attr)
        setattr(self, attr, value)

    def __init__(self, duration_ms=1000):
        GstBase.BaseTransform.__init__(self)
        self.duration_ms = duration_ms

        #wrap c library simd so we can use it from python
        #self.simd = ctypes.CDLL("libSimd.so")

    def do_transform_ip(self, buff):
        # get the current buffer, put inside a numpy array and use it in a simd lib op

        #(succ, width) = self.sinkpad.get_current_caps().get_structure(0).get_int("width")
        #(succ, height) = self.sinkpad.get_current_caps().get_structure(0).get_int("height") if succ else (False, 0)
        #(succ, mapinfo) = buff.map(Gst.MapFlags.READ) if succ else (False, None)
        ##if succ:
        #    arr = numpy.fromstring(mapinfo.data, numpy.uint8, count=width * height)
        #    arr_p = arr.ctypes.data_as(ctypes.POINTER(ctypes.c_int8))
        #    m = ctypes.c_int64()

        #    self.simd.SimdSobelDxAbsSum(arr_p, width, width, height, ctypes.byref(m))

        #buffer.duration = self.duration_ms * 1000000
        return Gst.FlowReturn.OK

GObject.type_register(Duration)
__gstelementfactory__ = ("duration", Gst.Rank.NONE, Duration)
